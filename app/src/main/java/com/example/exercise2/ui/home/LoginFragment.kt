package com.example.exercise2.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.exercise2.R

class LoginFragment : Fragment(),View.OnClickListener {

    lateinit var btnCancelLogin:Button
    lateinit var btnEnterLogin:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnCancelLogin=view.findViewById(R.id.btnCancelLogin)
        btnCancelLogin.setOnClickListener(this)

        btnEnterLogin=view.findViewById(R.id.btnEnterLogin)
        btnEnterLogin.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        if (v==btnCancelLogin){
            findNavController().navigate(R.id.navigation_home)
        }

        else if (v==btnEnterLogin){

        }

    }
}
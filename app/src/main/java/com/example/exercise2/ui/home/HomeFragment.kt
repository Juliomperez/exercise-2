package com.example.exercise2.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.exercise2.R

class HomeFragment : Fragment(),View.OnClickListener {

    private lateinit var homeViewModel: HomeViewModel

    lateinit var btnLogin:Button
    lateinit var btnSingin:Button

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(R.id.textView)
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin=view.findViewById(R.id.btnLogin)
        btnLogin.setOnClickListener(this)

        btnSingin=view.findViewById(R.id.btnSingin)
        btnSingin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v==btnLogin){
            findNavController().navigate(R.id.action_navigation_home_to_loginFragment)
        }

        else if (v==btnSingin){
            findNavController().navigate(R.id.action_navigation_home_to_registerFragment)
        }

    }


}
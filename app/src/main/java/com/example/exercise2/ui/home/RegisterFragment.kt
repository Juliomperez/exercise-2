package com.example.exercise2.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.exercise2.R

class RegisterFragment : Fragment(),View.OnClickListener {

    lateinit var btnCancelSingin:Button
    lateinit var btnEnterSingin:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnCancelSingin=view.findViewById(R.id.btnCancelSingin)
        btnCancelSingin.setOnClickListener(this)

        btnEnterSingin=view.findViewById(R.id.btnEnterSingin)
        btnEnterSingin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v==btnCancelSingin){
            findNavController().navigate(R.id.navigation_home)
        }

        else if (v==btnEnterSingin){

        }

    }

}